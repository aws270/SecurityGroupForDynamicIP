import json
import socket
from pprint import pprint
import boto3
from datetime import date

def lambda_handler(event, context):
    ec2 = boto3.resource('ec2')
    sg = ec2.SecurityGroup('sg-yourSecurityGroupToEdit')
    
    # EXAMPLE OF LIST OF RULES
    # rulesList = [
    #     {'domain': 'email-smtp.eu-central-1.amazonaws.com', 'ports': [22, 33, 44], 'protocol': 'tcp'},
    #     {'domain': 'google.it', 'ports': [77, 78, 79], 'protocol': 'tcp'},
    #     {'domain': 'wikipedia.org', 'ports': [97, 98, 99], 'protocol': 'udp'},
    # ]

    rulesList = [
        {'domain': 'email-smtp.eu-central-1.amazonaws.com', 'ports': [25, 587, 2587, 465, 2465], 'protocol': 'tcp'},
    ]
    rules = resolveHostname(rulesList)
    remove_old_outbound_rules(sg)
    add_outbound_rules(sg, rules)
    
    response = {'Status':'Success'}
    return response
    
def resolveHostname(rulesList):
    domain = ''
    ips = ''
    rules = list()
    print("------ RULES LIST ------")
    for rule in rulesList:
        domain = rule['domain']
        protocol = rule['protocol']
        ips = socket.gethostbyname_ex(domain)
        ports = rule['ports']
        print(domain, " -> ", ips[2])
        print("Port -> ", ports, " -> ", protocol)
        print("--------")
        for ip in ips[2]:
            for port in ports:
                rule = {
                    "IpProtocol": protocol,
                    "FromPort": port,
                    "ToPort": port,
                    "IpRanges": [
                        {
                          "CidrIp": ip+"/32",
                          "Description": "Automatically added by Lambda function on "+str(date.today())
                        }
                    ]
                }
                rules.append(rule)
        
    return rules
    
def add_outbound_rules(sg, rules):
    add = sg.authorize_egress(DryRun=False, IpPermissions=rules)
    #print(add)
    return ""

def remove_old_outbound_rules(sg):
    if (len(sg.ip_permissions_egress) > 0 ):
        #print(sg.ip_permissions_egress, type(sg.ip_permissions_egress))
        remove = sg.revoke_egress(DryRun=False,IpPermissions=sg.ip_permissions_egress)
        return ""
    else:
        print('No rules to remove')
        return ""

        
    return {
        'statusCode': 200,
        'body': json.dumps(response)
        
    }

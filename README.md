# Security Group For Dynamic IP



## What is it for?

This snippet allows you to create a Lambda function that can automatically modify your AWS Security Group.
It can be useful if you want to define rules for specific ports towards services that have one or more dynamic IPs without having to make a rule towards 0.0.0.0

## What should I do first?
[AWS Doc](https://aws.amazon.com/blogs/security/how-to-automatically-update-your-security-groups-for-amazon-cloudfront-and-aws-waf-by-using-aws-lambda/)

## How to automatically run my Lambda function?
[AWS Doc](https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/RunLambdaSchedule.html)

## License
Do what you want with it, I'll be happy.

